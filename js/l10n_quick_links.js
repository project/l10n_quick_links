(function ($, cookies) {

  'use strict';

  Drupal.behaviors.l10nQuickLinks = {
    attach: function () {
      let input = '';
      let $filter = $('.l10n-quick-links-search');
      if ($filter.length > 0) {
        $filter.on("input", function() {
          input = $(this).val().trim();
          if (input.length > 0) {
            $('.l10n-quick-links-container li').hide();
            $('.l10n-quick-links-container li:contains("'+input+'")').show();
          }
          else {
            $('.l10n-quick-links-container li').show();
          }
        });
      }
    }
  };

})(jQuery, window.Cookies);
