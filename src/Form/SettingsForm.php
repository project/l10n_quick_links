<?php

namespace Drupal\l10n_quick_links\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the localization quick links module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SettingsForm constructor
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'l10n_quick_links_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['l10n_quick_links.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('l10n_quick_links.settings');

    $form['disabled_paths'] = [
      '#title' => t('Disable on-page translation on the following system paths'),
      '#type' => 'textarea',
      '#description' => t('One per line. Wildcard-enabled. Examples: system/ajax, admin*'),
      '#default_value' => $config->get('disabled_paths'),
    ];

    $entity_types_options = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      // Only track content entity types.
      if ($definition->entityClassImplements(ContentEntityInterface::class)) {
        $entity_types_options[$definition->id()] = $definition->getLabel();
      }
    };
    $form['entity_types'] = [
      '#title' => $this->t('Entity type fields'),
      '#type' => 'checkboxes',
      '#options' => $entity_types_options,
      '#description' => $this->t('Track fields from selected entity types to have quick links to the translate page.<br />They are tracked on display and form. Render caching will be disabled automatically when the widget is visible.'),
      '#default_value' => $config->get('entity_types')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('l10n_quick_links.settings')
      ->set('disabled_paths', $form_state->getValue('disabled_paths'))
      ->set('entity_types', $form_state->getValue('entity_types'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
