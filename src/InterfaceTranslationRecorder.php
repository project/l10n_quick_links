<?php

namespace Drupal\l10n_quick_links;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

/**
 * String translation listener to collect data.
 *
 * Inspired by the l10n_client_ui project.
 */
class InterfaceTranslationRecorder implements TranslatorInterface  {

  /**
   * Strings that were attempted to be looked up in this request.
   *
   * @var array
   */
  protected $strings = array();

  /**
   * Strings that were loaded in this request.
   *
   * @var array
   */
  protected $fields = array();

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    if ($langcode != 'en' || locale_is_translatable('en')) {

      $add = TRUE;
      // Ignore strings which are not useful at all like toolbar menu links,
      // bundle labels and annotations. A few might still slip through, but
      // that's usually fine.
      $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
      foreach ($backtrace as $item) {
        if (in_array($item['function'],  ['preRenderToolbar', 'template_preprocess_toolbar', 'getBundleLabel', 'Annotation'])) {
          $add = FALSE;
          break;
        }
      }

      if ($add) {
        $this->strings[$langcode][$context][$string] = TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Adds a field to the strings.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\Core\Field\FieldConfigInterface $fieldDefinition
   * @param $langcode
   */
  public function addField(EntityInterface $entity, FieldConfigInterface $fieldDefinition, $langcode) {
    $this->fields[$fieldDefinition->id()] = $fieldDefinition;
    $this->strings[$langcode]['_field_definition'][$fieldDefinition->id()] = [
      'label' => $fieldDefinition->label(),
      'entity_type_id' => $fieldDefinition->getTargetEntityTypeId(),
      'bundle_key' => $entity->getEntityType()->hasKey('bundle') ? $entity->getEntityType()->getBundleEntityType() ?: $entity->getEntityType()->getKey('bundle') : '',
      'bundle_id' => $fieldDefinition->getTargetBundle(),
      'base_field' => !($fieldDefinition instanceof \Drupal\field\FieldConfigInterface)
    ];
  }

  /**
   * @inheritdoc
   */
  public function reset() {
    $this->strings = array();
  }

  /**
   * Return the record data.
   *
   * @return array
   *   Array of strings keyed by language code and context.
   */
  public function getRecordedData() {
    return $this->strings;
  }

}
