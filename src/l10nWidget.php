<?php

namespace Drupal\l10n_quick_links;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\locale\StringStorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class l10nWidget implements l10nWidgetInterface {

  /**
   * @var \Drupal\l10n_quick_links\InterfaceTranslationRecorder
   */
  protected $interfaceTranslationRecorder;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The locale string storage.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localeStorage;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack $requestStack
   */
  protected $requestStack;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current language id.
   *
   * @var string
   */
  protected $currentLanguageId;

  /**
   * l10nWidget constructor
   *
   * @param \Drupal\l10n_quick_links\InterfaceTranslationRecorder $interface_translation_recorder
   *   The interface translation recorder.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\locale\StringStorageInterface $locale_storage
   *   The locale string storage.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(InterfaceTranslationRecorder $interface_translation_recorder, LanguageManagerInterface $language_manager, StringStorageInterface $locale_storage, RequestStack $request_stack, AccountInterface $current_user, PathMatcherInterface $path_matcher, CurrentPathStack $current_path, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->interfaceTranslationRecorder = $interface_translation_recorder;
    $this->languageManager = $language_manager;
    $this->localeStorage = $locale_storage;
    $this->requestStack = $request_stack;
    $this->currentUser = $current_user;
    $this->pathMatcher = $path_matcher;
    $this->currentPath = $current_path;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function addWidget(array &$variables) {

    if (!$this->hasAccess()) {
      return;
    }

    // Get recorded interface translation data.
    $strings = $this->interfaceTranslationRecorder->getRecordedData();

    // Current language id.
    $this->currentLanguageId = $this->languageManager->getCurrentLanguage()->getId();

    $links = [];
    if (count($strings)) {
      foreach ($strings as $langcode => $contexts) {
        foreach ($contexts as $context => $string_list) {

          $string_links = [];
          foreach ($string_list as $string => $target) {
            // Base and configurable fields.
            // Base field labels are translated with t().
            if ($context == '_field_definition') {
              if ($target['base_field']) {
                $string_links[] = $this->getLocaleTranslationLink($langcode, $target['label'], '');
              }
              else {
                $bundle_key = $target['bundle_key'];
                $bundle_id = $target['bundle_id'];
                $entity_type_id = $target['entity_type_id'];
                $route_parameters = [
                  'field_config' => $string,
                ];
                if (!empty($bundle_key)) {
                  $route_parameters[$bundle_key] = $bundle_id;
                }
                $string_links[] = [
                  'url' => Url::fromRoute('entity.field_config.config_translation_overview.' . $entity_type_id, $route_parameters),
                  'title' => $target['label'],
                  'attributes' => [
                    'target' => 'blank',
                  ],
                ];
              }
            }
            // Regular t() strings.
            else {
              $string_links[] = $this->getLocaleTranslationLink($langcode, $string, $context);
            }
          }
          if (!empty($string_links)) {
            $context_label = empty($context) ? 'General' : $context;
            if ($context == '_field_definition') {
              $context_label = 'Field';
            }
            $links[] = [
              '#theme' => 'links',
              '#heading' => 'Context: ' . $context_label,
              '#links' => $string_links,
            ];
          }
        }
      }
    }

    if (!empty($links)) {
      $container_class = ' closed';
      $toggle_label = 'Translate';
      if (isset($_COOKIE['l10nQuickLinks'])) {
        $toggle_label = 'Close';
        $container_class = ' open';
      }

      $attached = [
        '#attached' => [
          'library' => ['l10n_quick_links/l10n_quick_links'],
        ],
      ];
      $variables['page']['content'][] = $attached;
      $variables['page']['content'][] = [
        '#theme' => 'l10n_quick_links_widget',
        '#links' => $links,
        '#toggle_label' => $toggle_label,
        '#container_class' => $container_class,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccess($module = NULL) {
    $access = FALSE;

    // Disable for CLI.
    if (PHP_SAPI === 'cli') {
      return $access;
    }

    // Check if the module is enabled if $module is passed into the access
    // method. Defaults to TRUE if nothing is passed.
    $module_access = !isset($module) || $this->moduleHandler->moduleExists($module);
    // config_translation also needs Field UI to be enabled.
    if ($module && $module == 'config_translation' && $module_access) {
      $module_access = $this->moduleHandler->moduleExists('field_ui');
    }

    if (
      $this->currentUser->hasPermission('use localization quick links ui') &&
      !$this->pathMatcher->matchPath($this->currentPath->getPath($this->requestStack->getCurrentRequest()), $this->configFactory->get('l10n_quick_links.settings')->get('disabled_paths')) &&
      $module_access) {
      $access = TRUE;
    }

    return $access;
  }

  /**
   * Get a link to the translation UI.
   *
   * @param $langcode
   * @param $source
   * @param $context
   *
   * @return array
   */
  protected function getLocaleTranslationLink($langcode, $source, $context) {
    $translation = $this->localeStorage->findTranslation(array(
      'language' => $langcode,
      'source' => $source,
      'context' => $context,
    ));
    $text_string = !empty($translation->translation) ? $translation->translation : $source;
    $url_options = [
      'query' => [
        'string' => $source,
        'langcode' => $this->currentLanguageId,
      ],
    ];
    return [
      'url' => Url::fromRoute('locale.translate_page', [], $url_options),
      'title' => $text_string,
      'attributes' => [
        'target' => 'blank',
      ],
    ];
  }

}
