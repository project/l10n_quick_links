<?php

namespace Drupal\l10n_quick_links;

interface l10nWidgetInterface {

  /**
   * Adds l10n quick links widget to the page.
   *
   * @param array $variables
   *   The variables for the page.
   */
  public function addWidget(array &$variables);

  /**
   * Returns whether the current user can access the on-page translation tool.
   *
   * @param $module
   *   The module that needs to be enabled, e.g. config_translation
   *
   * @return bool
   */
  public function hasAccess($module = NULL);
}
