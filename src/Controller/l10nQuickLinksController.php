<?php

namespace Drupal\l10n_quick_links\Controller;

use Drupal\Core\Controller\ControllerBase;

class l10nQuickLinksController extends ControllerBase {

  /**
   * Toggle the l10n widget.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function toggleWidget() {

    $expire = time() + (86400 * 365);
    if (!isset($_COOKIE[L10N_COOKIE_NAME])) {
      setcookie('l10nQuickLinks', 'show', $expire, '/');
    }
    else {
      setcookie('l10nQuickLinks', '', $expire, '/');
    }

    // Redirect to front, don't use <current> so we don't end up in an infinite
    // recursion. Normally, the destination string should be there in the
    // toolbar link.
    return $this->redirect('<front>');
  }

}
