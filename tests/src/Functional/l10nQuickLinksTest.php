<?php

namespace Drupal\Tests\l10n_quick_links\Functional;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests l10n quick links.
 *
 * @group l10n_quick_links
 */
class l10nQuickLinksTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
    'config_translation',
    'locale',
    'node',
    'user'
  ];

  /**
   * The default theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with all permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    try {
      $this->adminUser = $this->createUser([], 'administrator', TRUE);
    } catch (EntityStorageException $ignored) {}
  }

  /**
   * Tests l10n quick links.
   */
  public function testL10nQuickLinks() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    // Add languages.
    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm(['predefined_langcode' => 'nl'], 'Add language');
    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm(['predefined_langcode' => 'fr'], 'Add language');

    // Now enable l10n quick links. It should not crash to begin with.
    $edit = ['modules[l10n_quick_links][enable]' => TRUE];
    $this->drupalGet('admin/modules');
    $this->submitForm($edit, 'Install');
    $this->submitForm([], 'Continue');
    $assert_session->statusCodeEquals(200);

    $this->drupalGet('admin/config/regional/translate/l10n-quick-links');
    $assert_session->statusCodeEquals(200);
  }

}
